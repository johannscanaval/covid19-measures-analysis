# Analysis code
This folder contains all code used to analyse the Covid-19 pandemy data from OWID, Oxford and Google databases. 
## File codes:
1. org_data.ipynb - Jupyter notebook to organize, compile and export data. 
2. org_data~.csv - Exported files with data to analyze.
3. poisson_reg.R - R code with analysis from Google's data. 
4. poisson_reg_categ.R -  R code with analysis from Oxford data.
5. poisson_reg_categ.R  - R code with analysis from Oxford data (time percent of the measure - Binomial regression 2).