#Quitar nans
library(tidyr)
org_data_categ_timeline <- org_data_categ_timeline %>%
  drop_na()

#Poisoon
poisson.model1p <- glm(Deaths~C10+C11+C12+C13+C20+C21+C22+C23+C30+C31+C32+C40+C41+C42+C43+C44+C50+C51+C52+C60+C61+C62+C63
                      +C70+C71+C72+C80+C81+C82+C83+C84+offset(logpop),org_data_categ_timeline, family = poisson(link="log"))
summary(poisson.model1p)
fitted(poisson.modelp)
library(DescTools)
PseudoR2(poisson.model1p, "Nagelkerke")
influence.measures(poisson.model1p)


# Binomial negativa
library(MASS)
poisson.model2p <- glm.nb(Deaths~C10+C11+C12+C13+C20+C21+C22+C23+C30+C31+C32+C40+C41+C42+C43+C44+C50+C51+C52+C60+C61+C62+C63+C70+C71+C72+C80+C81+C82+C83+C84+offset(logpop),data=org_data_categ_timeline)
summary(poisson.model2p)
PseudoR2(poisson.model2p, "Nagelkerke")
influence.measures(poisson.model2p)

#Pseudo poisson
poisson.model3p <- glm(Deaths~C10+C11+C12+C13+C20+C21+C22+C23+C30+C31+C32+C40+C41+C42+C43+C44+C50+C51+C52+C60+C61+C62+C63
                       +C70+C71+C72+C80+C81+C82+C83+C84+offset(logpop),org_data_categ_timeline, family = quasipoisson(link="log"))
summary(poisson.model3p)
PseudoR2(poisson.model1p, "Nagelkerke")
influence.measures(poisson.model1p)
