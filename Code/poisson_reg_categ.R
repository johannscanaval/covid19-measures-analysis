org_data_categ <- read.csv("C:/Users/johan/OneDrive - Universidad de los Andes/F�sica/2021-2/Proyecto te�rico/Proyecto/Code/org_data_categ.csv")
#Quitarnans
library(tidyr)
library(car)
library(lmtest)
library(MASS)
org_data_categ <- org_data_categ %>%
  drop_na()
#Categorizar
org_data_categ$C1 <- factor(org_data_categ$C1, ordered = TRUE, levels = c(0,1,2,3))
org_data_categ$C2 <- factor(org_data_categ$C2, ordered = TRUE, levels = c(0,1,2,3))
org_data_categ$C3 <- factor(org_data_categ$C3, ordered = TRUE, levels = c(0,1,2))
org_data_categ$C4 <- factor(org_data_categ$C4, ordered = TRUE, levels = c(0,1,2,3,4))
org_data_categ$C5 <- factor(org_data_categ$C5, ordered = TRUE, levels = c(0,1,2))
org_data_categ$C6 <- factor(org_data_categ$C6, ordered = TRUE, levels = c(0,1,2,3))
org_data_categ$C7 <- factor(org_data_categ$C7, ordered = TRUE, levels = c(0,1,2))
org_data_categ$C8 <- factor(org_data_categ$C8, ordered = TRUE, levels = c(0,1,2,3,4))
#Sin cat
org_data_categ$C1 <- factor(org_data_categ$C1, ordered = FALSE, levels = c(0,1,2,3))
org_data_categ$C2 <- factor(org_data_categ$C2, ordered = FALSE, levels = c(0,1,2,3))
org_data_categ$C3 <- factor(org_data_categ$C3, ordered = FALSE, levels = c(0,1,2))
org_data_categ$C4 <- factor(org_data_categ$C4, ordered = FALSE, levels = c(0,1,2,3,4))
org_data_categ$C5 <- factor(org_data_categ$C5, ordered = FALSE, levels = c(0,1,2))
org_data_categ$C6 <- factor(org_data_categ$C6, ordered = FALSE, levels = c(0,1,2,3))
org_data_categ$C7 <- factor(org_data_categ$C7, ordered = FALSE, levels = c(0,1,2))
org_data_categ$C8 <- factor(org_data_categ$C8, ordered = FALSE, levels = c(0,1,2,3,4))

summary(org_data_categ)
#Modelo 1: Poisson
poisson.model1c <- glm(Deaths~C1+C2+C3+C4+C5+C6+C7+C8+offset(Logpop),org_data_categ, family = poisson(link="log"))
summary(poisson.model1c)
fitted(poisson.modelc)
library(DescTools)
PseudoR2(poisson.model1c, "Nagelkerke")
influence.measures(poisson.model1) #muchos puntos influyentes

#Modelo 2: Binomial negativa
library(MASS)
poisson.model2c <- glm.nb(Deaths~C1+C2+C3+C4+C5+C6+C7+C8++offset(Logpop),data=org_data_categ)
summary(poisson.model2c)
PseudoR2(poisson.model2c, "Nagelkerke")
influence.measures(poisson.model2)
VIF(poisson.model2)

#sacar inf
org_data_categf <- org_data_categ[-c(15,24,45,52,62,77,78), ]
poisson.model2d <- glm.nb(Deaths~C1+C2+C3+C4+C5+C6+C7+C8+offset(Logpop),data=org_data_categf)
summary(poisson.model2d)
influence.measures(poisson.model2d)
PseudoR2(poisson.model2d, "Nagelkerke")

#robust 
library(multinbmod)
poisson.model3 <- multinbmod(Deaths~C1+C2+C3+C4+C5+C6+C7+C8+fullyvac+offset(Logpop),data=org_data_categ)
summary(poisson.model3)

library(robust)
poisson.model4a<- glmRob(Deaths~C1+C2+C3+C5+C7+C8+fullyvac+offset(Logpop),org_data_categ, family = poisson(), method='cubif')
summary(poisson.model4a)

#cond index
library(klaR)
conditionM<- cond.index(Deaths~C1+C2+C3+C4+C5+C6+C7+C8+offset(Logpop),data=org_data_categ)
conditionM

#grafica de correlacion
library(corrplot)
variables <- c('C1','C2','C3','C4','C5','C6','C7','C8', 'Logpop', 'fullyvac')
matriz <- org_data_categ[variables]
corrplot(matriz)
summary(matriz)

#Modelo 5: Binomial negativa sin C6

poisson.model5 <- glm.nb(Deaths~C1+C2+C3+C4+C5+C7+C8+offset(Logpop),data=org_data_categ)
summary(poisson.model5)
PseudoR2(poisson.model5, "Nagelkerke")
influence.measures(poisson.model5)
VIF(poisson.model5)

#sin puntos influyentes
org_data_categf2 <- org_data_categ[-c(14,17,26,31), ]
poisson.model5f <- glm.nb(Deaths~C1+C2+C3+C4+C5+C7+C8+fullyvac+offset(Logpop),data=org_data_categf2)
summary(poisson.model5f)
PseudoR2(poisson.model5f, "Nagelkerke")
influence.measures(poisson.model5f)

modelstd <- rstudent(poisson.model5f)
dfbetaPlots(poisson.model5f)

#Modelo 6: Binomial negativa SOLO CON C6

poisson.model6 <- glm.nb(Deaths~C6+fullyvac+offset(Logpop),data=org_data_categ)
summary(poisson.model6)
PseudoR2(poisson.model6, "Nagelkerke")
influence.measures(poisson.model6)
VIF(poisson.model6)

#Modelo 7: Binomial negativa sin C6+ C3 XC4

poisson.model7 <- glm.nb(Deaths~C1+C2+C3*C4+C5+C7+C8+offset(Logpop),data=org_data_categ)
summary(poisson.model7)
PseudoR2(poisson.model7, "Nagelkerke")
influence.measures(poisson.model7)
VIF(poisson.model7)

poisson.model7a2<- glm.nb(Deaths~C1+C2+C3+C4+I[C3*C4]+C5+C7+C8+fullyvac+offset(Logpop),org_data_categ)
summary(poisson.model7a2)


#Modelo 8: Binomial negativa sin C6 y con C3XC4 y C5XC4

poisson.model8 <- glm.nb(Deaths~C1+C2+C3*C4+C5+C4*C5+C7+C8+fullyvac+offset(Logpop),data=org_data_categ)
summary(poisson.model8)
PseudoR2(poisson.model8, "Nagelkerke")
influence.measures(poisson.model8)
VIF(poisson.model8) #NOPE

#Modelo 9: Binomial negativa sin C6 y C4
poisson.model9 <- glm.nb(Deaths~C1+C2+C3+C5+C7+C8+fullyvac+offset(Logpop),data=org_data_categ)
summary(poisson.model9)
PseudoR2(poisson.model9, "Nagelkerke")
influence.measures(poisson.model9)
VIF(poisson.model9) #Empeora

#Modelo 10: Simplificado
poisson.model10 <- glm.nb(Deaths~C1+C3*C4+C7+fullyvac+offset(Logpop),data=org_data_categ)
summary(poisson.model10)
PseudoR2(poisson.model10,"Nagelkerke")
influence.measures(poisson.model10)

org_data_categ10SPI <- org_data_categ[-c(4,14,26,44), ]
poisson.model10SPI <- glm.nb(Deaths~C1+C3*C4+C7+fullyvac+offset(Logpop),data=org_data_categ10SPI)
summary(poisson.model10SPI)
influence.measures(poisson.model10SPI)

org_data_categ10SPI2 <- org_data_categ10SPI[-c(35), ]
poisson.model10SPI2 <- glm.nb(Deaths~C1+C3*C4+C7+fullyvac+offset(Logpop),data=org_data_categ10SPI2)
summary(poisson.model10SPI2)
influence.measures(poisson.model10SPI2)

#Modelo 11, ANTES DE VACUNACIONES

org_data_categ_untilvac <- org_data_categ_untilvac %>%
  drop_na()

#11a poisson
poisson.model11a <- glm(Deaths~C1+C2+C3+C4+C5+C6+C7+C8+offset(Logpop),org_data_categ_untilvac, family = poisson(link="log"))
summary(poisson.model11a)

#11b binoneg
poisson.model11b <- glm.nb(Deaths~C1+C2+C3+C4+C5+C6+C7+C8+offset(Logpop),data=org_data_categ_untilvac)
summary(poisson.model11b)
PseudoR2(poisson.model11b,"Nagelkerke")
influence.measures(poisson.model11b)

#without vacc
#Modelo 12: Binomial negativa sin C6 y C4
poisson.model12 <- glm.nb(Deaths~C1+C2+C3*C4+C5+C7+C8+offset(Logpop),data=org_data_categ)
summary(poisson.model12)
PseudoR2(poisson.model12, "Nagelkerke")
influence.measures(poisson.model12)

org_data_categ12SPI <- org_data_categ[-c(17,26,27,31,9,14), ]
poisson.model12SPI <- glm.nb(Deaths~C1+C2+C3*C4+C5+C7+C8+offset(Logpop),data=org_data_categ12SPI)
summary(poisson.model12SPI)
PseudoR2(poisson.model12SPI, "Nagelkerke")
influence.measures(poisson.model12SPI)


