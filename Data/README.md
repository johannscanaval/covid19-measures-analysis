# Data Sources
All the data of this folder was upgraded (cloned) on 20/11/2021 and comes from:
1. [Google - COVID-19 Community Mobility Reports] (https://www.google.com/covid19/mobility/)
2. [Oxford Covid-19 Government Response Tracker (OxCGRT)] (https://github.com/OxCGRT/covid-policy-tracker) 
3. [COVID-19 Dataset by Our World in Data] (https://github.com/owid/covid-19-data)
